Install firebase
    npm install firebase

Install firebase tools
    npm install -g firebase-tools

Commands
    firebase login
    firebase projects:list    
    firebase init
    firebase deploy


If you are getting 403 forbidden error:

Please follow below steps to grant access to all users. Basically this is to allow unauthenticated clients to access your api endpoint.

Go to https://console.cloud.google.com/functions/list
Select the function to which you want to give public access
Click on PERMISSIONS
Click on ADD MEMBER
Type allUsers
Select role Cloud Functions -> Cloud Functions Invoker
Save
That's it, now test your api.
