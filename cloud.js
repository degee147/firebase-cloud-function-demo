// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBXS_eJ2GS8cSkU4c6GWMHU0YnDGG3gDzI",
  authDomain: "hive-fd1b2.firebaseapp.com",
  databaseURL: "https://hive-fd1b2-default-rtdb.firebaseio.com",
  projectId: "hive-fd1b2",
  storageBucket: "hive-fd1b2.appspot.com",
  messagingSenderId: "473813142166",
  appId: "1:473813142166:web:fe291dfdd5c9f7e93a90b7",
  measurementId: "G-J82B9KWGF1"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);