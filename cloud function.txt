const functions = require("firebase-functions");
const admin = require("firebase-admin");

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions

admin.initializeApp();

exports.accountSetUp = functions.https.onCall((data, context)=>{
  
  let validation = true;
  if (data.company == "" || data.company == null || data.company == undefined) {
    validation = false;
  }
  if (data.name == "" || data.name == null || data.name == undefined) {
    validation = false;
  }

  const expression = /^[^@]+@\w+(\.\w+)+\w$/;
  if (!expression.test(data.email)) {
    validation = false;
  }
  if (data.password.length < 8) {
    validation = false;
  }


  if (data.type == "employer" && validation == true) {
    const user = {
      email: data.email,
      emailVerified: false,
      password: data.password,
      displayName: data.name,
    };

    admin.auth().createUser(user
    ).then((userReturn) => {
      const companyDetails = {
        admin: userReturn.uid,
        company_name: data.company,
        signed: {
          sign: false,
          name: "",
          job: "",
          date: "",
          terms: "",
          signature: "",
        },
        profile: {
          size: "",
          location: "",
          logo: "",
          li: "",
          website: "",
          company_description: "",
        },
      };
      admin.database().ref("/employers").push(companyDetails)
          .then((companyReturn)=>{
            const userDetails={
              admin: true,
              manager_num: userReturn.uid,
              type: "employer",
              email: data.email,
              manager_name: data.name,
              profile: {
                job_title: "",
                phone: "",
                li: "",
                location: ""},
              company_num: companyReturn.key};


            admin.database().ref("/users/"+userReturn.uid).set(userDetails)
                .then(()=>{
                  return ("success");
                }).catch((error)=>{
                  return (error);
                });
          }).catch((error)=>{


          });

      // add to user database
      // add to company database
    }).catch((error)=>{
        
      return (error);
    });
  }
});

// (cd functions && npx eslint . --fix)
// cd ..\
//rate limiter.

//how to see return in frontend?
